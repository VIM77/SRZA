<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'app-name' => 'Служба релейной защиты и автоматики',
    'logged' => 'Данное приложение разработано для учета электрооборудования находящегося в обслуживании Службы релейной защиты и автоматики СР-3 ЦЭЭ',
    'not-logged' => 'Для работы в приложении необходимо авторизоваться',
    'dashboard' => 'База данных оборудования Службы релейной защиты и автоматики',

];
