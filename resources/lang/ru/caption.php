<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'app-name' => 'СРЗА-ЭТЛ',
    'empty' => 'Список пуст',
    'btnSave' => 'Сохранить',
    'btnReset' => 'Восстановить',
    'btnCancel' => 'Отменить',

    'locations' => 'Локации',
    'new-location' => 'Новая локация',
    'edit-location' => 'Редактирование локации',
    'show-location' => 'Локация',
    'show-objects' => 'Объекты',

    'units' => 'Объекты',
    'new-unit' => 'Новый объект',
    'edit-unit' => 'Редактирование объекта',
    'show-unit' => 'Объект',
    'show-cells' => 'Ячейки',

    'cells' => 'Ячейки',
    'new-cell' => 'Новая ячейка',
    'edit-cell' => 'Редактирование ячейки',
    'show-cell' => 'Ячейка/панель',
    'show-equipments' => 'Оборудование',

    'equipments' => 'Оборудование',
    'new-equipment' => 'Новое оборудование',
    'edit-equipment' => 'Редактирование оборудования',

    // Locales for Dictionary model
    'dictionaries' => 'Справочники',
    'new-dictionary' => 'Новый справочник',
    'dictionary-edit-element' => 'Редактирование элемента справочника',
    'dictionary-new-element' => 'Создание элемента справочника',
    'dictionary-id' => 'ИД',
    'dictionary-name' => 'Наименование',

    'EquipmentType' => 'Тип оборудования',
    'new-EquipmentType' => 'Новый тип оборудования',

    'VoltageTransformer' => 'Трансформатор напряжения',
    'new-VoltageTransformer' => 'Новый ТН',

    'CurrentTransformer' => 'Трансформатор тока',
    'new-CurrentTransformer' => 'Новый ТТ',

    'CurrentClass' => 'Класс тока',
    'new-CurrentClass' => 'Новый класс тока',

    'VoltageClass' => 'Класс напряжения',
    'new-VoltageClass' => 'Новый класс напряжения',

    'voltage-transformer-name' => 'КТ',
    'current-transformer-name' => 'КТ',

    // Locales for Location model
    'location-id' => 'ИД',
    'location-name' => 'Наименование',
    'location-description' => 'Описание',
    'location-btnSave' => 'Сохранить',
    'location-btnReset' => 'Восстановить',
    'location-btnCancel' => 'Отменить',

    // Locales for Unit model
    'unit-id' => 'ИД',
    'unit-name' => 'Наименование',
    'unit-description' => 'Описание',
    'unit-btnSave' => 'Сохранить',
    'unit-btnReset' => 'Восстановить',
    'unit-btnCancel' => 'Отменить',

    // Locales for Cell model
    'cell-id' => 'ИД',
    'cell-number' => 'Номер',
    'cell-name' => 'Присоединение',
    'cell-description' => 'Описание',
    'cell-btnSave' => 'Сохранить',
    'cell-btnReset' => 'Восстановить',
    'cell-btnCancel' => 'Отменить',

    // Locales for Equipment model
    'equipment-id' => 'ИД',
    'equipment-number' => 'Заводской номер',
    'equipment-name' => 'Наименование',
    'equipment-mark' => 'Марка',
    'equipment-model' => 'Модель',
    'equipment-equipment_type' => 'Тип',
    'equipment-production_date' => 'Дата производства',
    'equipment-schema_label' => 'Обозначение на схеме',
    'equipment-ratio' => 'Коэф. трансформации',
    'equipment-voltage_class' => 'Класс напряжения',
    'equipment-current_class' => 'Класс тока',
    'equipment-description' => 'Примечание',
    'equipment-btnSave' => 'Сохранить',
    'equipment-btnReset' => 'Восстановить',
    'equipment-btnCancel' => 'Отменить',

];
