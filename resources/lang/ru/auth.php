<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'login' => 'Войти',
    'register' => 'Регистрация',
    'logout' => 'Выйти',
    'failed' => 'Данный не соответствуют ни одной записи.',
    'password' => 'Неверный пароль.',
    'throttle' => 'Превышен лимит попыток авторизации. Попробуйте через :seconds секунд.',
    'e-mail' => 'Электронный адрес',
    'Password' => 'Пароль',
    'remember-me' => 'Запомнить',
    'Login' => 'Логин',
    'forgot' => 'Забыли пароль?',

];
