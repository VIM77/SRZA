<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'app-name' => 'SRZA',
    'empty' => 'Empty list',
    'btnSave' => 'Save',
    'btnReset' => 'Reset',
    'btnCancel' => 'Cancel',

    'locations' => 'Locations',
    'new-location' => 'New location',
    'edit-location' => 'Edit location',
    'show-location' => 'Location',
    'show-objects' => 'Objects',

    'units' => 'Objects',
    'new-unit' => 'New object',
    'edit-unit' => 'Edit object',
    'show-unit' => 'Object',
    'show-cells' => 'Cells',

    'cells' => 'Cells',
    'new-cell' => 'New cell',
    'edit-cell' => 'Edit cell',
    'show-cell' => 'Cell',
    'show-equipments' => 'Equipments',

    'equipments' => 'Equipments',
    'new-equipment' => 'New equipment',
    'edit-equipment' => 'Edit equipment',

    // Locales for Dictionary model
    'dictionaries' => 'Dictionaries',
    'new-dictionary' => 'New dictionary',
    'dictionary-edit-element' => 'Edit of the dictionary element',
    'dictionary-new-element' => 'Create of the dictionary element',
    'dictionary-id' => 'Id',
    'dictionary-name' => 'Name',

    'EquipmentType' => 'Equipment type',
    'new-EquipmentType' => 'New type of equipment',

    'VoltageTransformer' => 'Voltage transformer',
    'new-VoltageTransformer' => 'New VT',

    'CurrentTransformer' => 'Current transformer',
    'new-CurrentTransformer' => 'New CT',

    'CurrentClass' => 'Current class',
    'new-CurrentClass' => 'New current class',

    'VoltageClass' => 'Voltage class',
    'new-VoltageClass' => 'New voltage class',

    'voltage-transformer-name' => 'Ratio',
    'current-transformer-name' => 'Ratio',

    // Locales for Location model
    'location-id' => 'Id',
    'location-name' => 'Name',
    'location-description' => 'Description',
    'location-btnSave' => 'Save',
    'location-btnReset' => 'Reset',
    'location-btnCancel' => 'Cancel',

    // Locales for Unit model
    'unit-id' => 'Id',
    'unit-name' => 'Name',
    'unit-description' => 'Description',
    'unit-btnSave' => 'Save',
    'unit-btnReset' => 'Reset',
    'unit-btnCancel' => 'Cancel',

    // Locales for Cell model
    'cell-id' => 'Id',
    'cell-number' => 'Number',
    'cell-name' => 'Name',
    'cell-description' => 'Description',
    'cell-btnSave' => 'Save',
    'cell-btnReset' => 'Reset',
    'cell-btnCancel' => 'Cancel',

    // Locales for Equipment model
    'equipment-id' => 'Id',
    'equipment-number' => 'Number',
    'equipment-name' => 'Name',
    'equipment-mark' => 'Mark',
    'equipment-model' => 'Model',
    'equipment-equipment_type' => 'Type',
    'equipment-production_date' => 'Production date',
    'equipment-schema_label' => 'Schema label',
    'equipment-ratio' => 'Transformation ratio',
    'equipment-voltage_class' => 'Voltage class',
    'equipment-current_class' => 'Current class',
    'equipment-description' => 'Description',
    'equipment-btnSave' => 'Save',
    'equipment-btnReset' => 'Reset',
    'equipment-btnCancel' => 'Cancel',
];
