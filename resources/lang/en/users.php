<?php

return [
    "list" => "View users list",
    "usersNavbarIcon" => "Users",
    "profileNavbarIcon" => "Profile",
    "cabinetNavbarIcon" => "Cabinet",
    'chatsNavbarIcon' => 'Chats',
    "profileCardHeader" => "User's data",
    "id" => "ID",
    "name" => "Name",
    "email" => "E-Mail Address",
    "nickname" => "Nickname",
    "save" => "Save",
    "cancel" => "Cancel",
    "dataSaved" => "Data saved",
    "dataNotSaved" => "Data not saved",
    "profile" => "Profile",
    "contacts" => "Contacts",
    'about' => "About me",
];
